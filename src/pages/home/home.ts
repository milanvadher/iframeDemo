import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Push, PushObject, PushOptions } from '@ionic-native/push';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // adURL = 'http://purecelibacy.org/'
  adURL = 'https://saffrony-524ca.firebaseapp.com/';
  constructor(private alertCtrl: AlertController, private push: Push, public navCtrl: NavController) {
    // this.push.hasPermission()
    //   .then((res: any) => {

    //     if (res.isEnabled) {
    //       console.log('We have permission to send push notifications');
    //       this.pushSetup();
    //     } else {
    //       console.log('We do not have permission to send push notifications');
    //     }

    //   });
  }

  pushSetup() {
    const options: PushOptions = {
      android: {
        // senderID: 903842818214
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = this.push.init(options);

    pushObject.on('notification').subscribe((notification: any) => {
      let alert = this.alertCtrl.create({
        title: 'Notification',
        message: notification.message,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Done',
            handler: () => {
              console.log('Saras...!!');
            }
          }
        ]
      });
      alert.present();
    });

    pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));

    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }

}
